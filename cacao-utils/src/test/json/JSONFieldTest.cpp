#include <gtest/gtest.h>

#include "Object.h"
#include "json/JSONObject.h"

using namespace cacao;

TEST(JSONFieldTest, shouldCreateJSONObject) {
    JSONField field("field", String("value"));
    ASSERT_EQ(field.getName(), String("field"));
    const Object* fieldValue = field.getValue();
    ASSERT_FALSE(fieldValue == nullptr);
    ASSERT_EQ(*fieldValue, String("value"));
    RELEASE(fieldValue);
}

TEST(JSONFieldTest, shouldBeEquals) {
    JSONField jsonField("fieldName", String("test"));
    ASSERT_EQ(jsonField, JSONField("fieldName", String("test")));
    ASSERT_EQ(jsonField.hashCode(), JSONField("fieldName", String("test")).hashCode());
    ASSERT_NE(jsonField, JSONField("otherFieldName", String("test")));
}

TEST(JSONFieldTest, shouldAssert) {
    JSONField jsonField("fieldName", String("test"));
    ASSERT_EQ(jsonField.getName(), String("fieldName"));
    Object* pVal = jsonField.getValue();
    ASSERT_EQ(*pVal, String("test"));
    jsonField = JSONField("otherField", String("toto"));
    ASSERT_EQ(jsonField.getName(), String("otherField"));
    RELEASE_WITH_VALUE(pVal, jsonField.getValue());
    ASSERT_EQ(*pVal, String("toto"));
    RELEASE(pVal);
}

TEST(JSONFieldTest, shouldConvertIntoString) {
    JSONField jsonField("fieldName", String("fieldValue"));
    const String& toString = jsonField.toString();
    ASSERT_EQ(toString, String("{\"fieldName\":\"fieldValue\"}"));
}



