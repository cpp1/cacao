#include "json/JSONObject.h"

namespace cacao {

    ///// Constructeur(s) :

    JSONField::JSONField() {
        this->name = "";
        this->value = nullptr;
    }

    JSONField::JSONField(const JSONField& jsonField) {
        this->name = jsonField.getName();
        this->value = jsonField.getValue();
    }

    JSONField::JSONField(const String& name, const Object* val) {
        this->name = name;
        this->value = (val == nullptr ? nullptr : val->clone());
    }

    JSONField::JSONField(const String& name, const Object& val) {
        this->name = name;
        this->value = val.clone();
    }

    ///// Destructeur

    JSONField::~JSONField() noexcept {
        RELEASE(this->value);
    }

    ///// Méthodes de la classe Object :

    bool JSONField::equals(const Object& object) const {
        if (this == &object) {
            return true;
        }
        if (!instanceOf<JSONField>(&object)) {
            return false;
        }
        const auto* pThat = dynamic_cast<const JSONField*>(&object);
        bool isOK = areEquals(&(this->name), &(pThat->name));
        isOK = isOK && areEquals(this->value, pThat->value);
        return isOK;
    }

    int JSONField::hashCode() const {
        if (this->value == nullptr) {
            return this->name.hashCode();
        } else {
            return hash(this->name, *(this->value));
        }
    }

    String JSONField::toString() const {
        String str;
        str = str + "{";
        str = str + "\"" + (this->name) + "\"";
        str =  str + ":";
        if (this->value == nullptr) {
            str = str + "null";
        } else {
            str = str + "\"" + (this->value->toString()) + "\"";
        }
        str = str + "}";
        return str;
    }

    Object* JSONField::clone() const {
        return new JSONField(*this);
    }

    ///// Définition des opérateurs :

    JSONField& JSONField::operator=(const JSONField& jsonField) {
        if (this == &jsonField) {
            return *this;
        }
        this->name = jsonField.getName();
        RELEASE_WITH_VALUE(this->value, jsonField.getValue());
        return *this;
    }



    ///// Getters & Setters :

    String JSONField::getName() const noexcept {
        return this->name;
    }

    Object* JSONField::getValue() const noexcept {
        return (this->value == nullptr ? nullptr : this->value->clone());
    }


}


