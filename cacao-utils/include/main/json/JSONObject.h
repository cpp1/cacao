#ifndef CACAO_JSONOBJECT_H
#define CACAO_JSONOBJECT_H

#include "Object.h"

#include "string/String.h"
#include "collection/list/List.h"

namespace cacao {

    class JSONField : public virtual Object {
    private:
        String name;
        Object *value;

    public:
        JSONField();
        JSONField(const String& name, const Object *val);
        JSONField(const String& name, const Object &val);
        JSONField(const JSONField &jsonField);
        ~JSONField() noexcept override;

    public:
        [[nodiscard]] bool equals(const Object &object) const override;
        [[nodiscard]] int hashCode() const override;
        [[nodiscard]] String toString() const override;
        [[nodiscard]] Object *clone() const override;

    public:
        JSONField& operator=(const JSONField &jsonField);

    public:
        [[nodiscard]] String getName() const noexcept;
        [[nodiscard]] Object* getValue() const noexcept;
    };

    class JSONObject : public virtual Object {


    };

}

#endif
