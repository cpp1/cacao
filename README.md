# cacao
## Description
Bibliothèque C++ regroupant des classes utilitaires pour le développement en C++.
## Installation
### Installation des outils de développement
#### Sous Linux
Pour le développement de cette librairie, les outils et librairies suivants sont nécessaires :
* Compilateur GCC et outils de base
~~~bash
sudo apt install build-essential -y
~~~
* Outil CMake pour la construction et la gestion des projets C++
~~~bash
sudo apt install cmake -y
~~~
* Installation de la librairie GTest de Google :
~~~bash
sudo apt install googletest-tools -y
~~~
* Outil Valgrind permettant de tester la mémoire :
~~~bash
sudo apt install valgrind -y
~~~
* Tester ensuite les installations en lançant les commandes suivantes :
~~~bash
g++ --version
make --version
cmake --version
valgrind --version
~~~
### Compilation du projet
Pour compiler le projet, il suffit de lancer les commandes suivants :
* Lancer la génération du Makefile : ``cmake .``
* Lancer les tests 
## Ressources documentaires
### Librairie GTest
Voici les différents liens concernant le framework GTest :
* [CMake et GTest](https://google.github.io/googletest/quickstart-cmake.html)
