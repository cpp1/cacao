#include <gtest/gtest.h>

#include "boolean/Boolean.h"
#include "exception/Exception.h"

namespace cacao {

    TEST(BooleanTest, shouldBuildFromInteger) {
        ASSERT_EQ(Boolean(0), Boolean::FALSE);
        ASSERT_EQ(Boolean(1), Boolean::TRUE);
        ASSERT_EQ(Boolean(2), Boolean::FALSE);
        ASSERT_EQ(Boolean(3), Boolean::TRUE);
    }

    TEST(BooleanTest, shouldBeEquals) {
        Boolean bool1(true);
        ASSERT_EQ(bool1, Boolean(true));
        ASSERT_TRUE(bool1.equals(Boolean(true)));
        ASSERT_EQ(bool1.hashCode(), Boolean(true).hashCode());
        ASSERT_NE(bool1, Boolean(false));
        ASSERT_TRUE(bool1 == Boolean::TRUE);
        ASSERT_TRUE(bool1.equals(Boolean::TRUE));
        ASSERT_TRUE(bool1 == true);
        ASSERT_TRUE(bool1 != Boolean::FALSE);
    }

    TEST(BooleanTest, shouldAssign) {
        Boolean bool1(true);
        bool1 = Boolean(false);
        ASSERT_TRUE(bool1 == Boolean::FALSE);
        bool1 = true;
        ASSERT_TRUE(bool1 == Boolean::TRUE);
    }
    
    TEST(BooleanTest, shouldUseOperators) {
        ASSERT_EQ(Boolean::TRUE && Boolean::TRUE, Boolean::TRUE);
        ASSERT_EQ(Boolean::TRUE && true, Boolean::TRUE);
        ASSERT_EQ(Boolean::TRUE && Boolean::FALSE, Boolean::FALSE);
        ASSERT_EQ(Boolean::FALSE && Boolean::TRUE, Boolean::FALSE);
        ASSERT_EQ(Boolean::FALSE && Boolean::FALSE, Boolean::FALSE);
        ASSERT_EQ(Boolean::TRUE || Boolean::TRUE, Boolean::TRUE);
        ASSERT_EQ(Boolean::TRUE || Boolean::FALSE, Boolean::TRUE);
        ASSERT_EQ(Boolean::FALSE || Boolean::TRUE, Boolean::TRUE);
        ASSERT_EQ(Boolean::FALSE || Boolean::FALSE, Boolean::FALSE);
        ASSERT_EQ(Boolean::FALSE || false, Boolean::FALSE);
    }

    TEST(BooleanTest, shouldNegate) {
        ASSERT_EQ(!Boolean::TRUE, Boolean::FALSE);
        ASSERT_EQ(!Boolean::FALSE, Boolean::TRUE);
    }
    
    TEST(BooleanTest, shouldClone) {
        Object* pClonedBool = Boolean::TRUE.clone();
        ASSERT_EQ(*pClonedBool, Boolean::TRUE);
        ASSERT_TRUE(*pClonedBool == Boolean::TRUE);
        FINALLY([&]() -> void {
            RELEASE(pClonedBool);
        });
    }

    TEST(BooleanTest, shouldUseArithmeticOpertions) {
        ASSERT_EQ(Boolean::TRUE + Boolean::TRUE, Boolean::FALSE);
        ASSERT_EQ(Boolean::FALSE + Boolean::TRUE, Boolean::TRUE);
        ASSERT_EQ(Boolean::TRUE + Boolean::FALSE, Boolean::TRUE);
        ASSERT_EQ(Boolean::TRUE * Boolean::TRUE, Boolean::TRUE);
        ASSERT_EQ(Boolean::TRUE * Boolean::FALSE, Boolean::FALSE);
        ASSERT_EQ(Boolean::FALSE * Boolean::TRUE, Boolean::FALSE);
        ASSERT_EQ(Boolean::FALSE - Boolean::TRUE, Boolean::TRUE);
        ASSERT_EQ(Boolean::TRUE - Boolean::FALSE, Boolean::TRUE);
        ASSERT_EQ(Boolean::TRUE - Boolean::TRUE, Boolean::FALSE);
        ASSERT_EQ(Boolean::FALSE - Boolean::FALSE, Boolean::FALSE);
    }

    TEST(BooleanTest, shouldConvertIntoBool) {
        ASSERT_TRUE(Boolean::TRUE.boolValue());
        ASSERT_FALSE(Boolean::FALSE.boolValue());
    }
}