#include "gtest/gtest.h"
#include "System.h"

using namespace cacao;

///// Tests unitaires :

TEST(SystemTest, shouldBeUnique) {
    const System& system1 = System::getInstance();
    const System& system2 = System::getInstance();
    ASSERT_EQ(system1, system2);
    ASSERT_EQ(system1.hashCode(), system2.hashCode());
    ASSERT_EQ(&system1, &system2);
}