#include <gtest/gtest.h>
#include "collection/Pair.h"
#include "string/String.h"

namespace cacao {

    TEST(PairTest, shouldGetValue) {
        Pair<String, String> pair("val1", "val2");
        ASSERT_EQ(pair.getLeftValue(), String("val1"));
        ASSERT_EQ(pair.getRightValue(), String("val2"));
    }

    TEST(PairTest, shouldBeEquals) {
        Pair<String, String> pair("val1", "val2");
        ASSERT_EQ(pair, pair);
        ASSERT_EQ(pair, Pair(String("val1"), String("val2")));
        ASSERT_NE(pair, Pair(String("val1"), String("otherValue")));
    }
    
    TEST(PairTest, createPair) {
        const Pair <String, String>& pair = Pair<String, String>::createPair("val1", "val2");
        ASSERT_EQ(pair.getLeftValue(), String("val1"));
        ASSERT_EQ(pair.getRightValue(), String("val2"));
    }
}
