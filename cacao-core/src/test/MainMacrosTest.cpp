
#include <gtest/gtest.h>

#include "MainMacros.h"
#include "string//String.h"

using namespace cacao;

TEST(MainMacrosTest, shoulReleaseTabWithValue) {
    int *tab = new int[]{5, 3};
    EXPECT_TRUE(tab[0] == 5);
    EXPECT_TRUE(tab[1] == 3);
    RELEASE_TAB_WITH_VALUE(tab, (new int[]{4, 6}));
    EXPECT_TRUE(tab[0] == 4);
    EXPECT_TRUE(tab[1] == 6);
    RELEASE_TAB(tab)
}

TEST(MainMacrosTest, shouldReleaseWithValue) {
    auto *str = new String("fred");
    EXPECT_TRUE(*str == String("fred"));
    RELEASE_WITH_VALUE(str, new String("laurent"));
    EXPECT_TRUE(*str == String("laurent"));
    RELEASE(str);
}







