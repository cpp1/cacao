#include <gtest/gtest.h>
#include "numeric/Integer.h"

using namespace cacao;

TEST(IntegerTest, shouldBeEquals) {
    Integer val(5);
    ASSERT_EQ(val, Integer(5));
    ASSERT_EQ(val.hashCode(), Integer(5).hashCode());
    ASSERT_NE(val, Integer(345));
    const Object* pClone = val.clone();
    ASSERT_EQ(*pClone, val);
    RELEASE(pClone);
}

TEST(IntegerTest, shouldAdd) {
    Integer val(6);
    val = val + 5;
    ASSERT_EQ(val, Integer(11));
    val -= 10;
    ASSERT_EQ(val, Integer(1));
}

TEST(IntegerTest, shouldMultiply) {
    Integer val(6);
    val = val * 5;
    ASSERT_EQ(val, Integer(30));
}

TEST(IntegerTest, shouldConvertFromString) {
    ASSERT_EQ(Integer::convertFromString(String("33")), Integer(33));
    ASSERT_EQ(Integer::convertFromString(String("678")), Integer(678));
    ASSERT_EQ(Integer::convertFromString(String("678")).toBaseInt(), 678);
}

