#include <gtest/gtest.h>

#include "exception/RuntimeException.h"

namespace cacao {
    
    TEST(RuntimeExceptionTest, shouldClone) {
        RuntimeException except("runtimeException");
        auto pCloneExcept = except.clone();
        ASSERT_EQ(except, *pCloneExcept);
        ASSERT_EQ(except.hashCode(), pCloneExcept->hashCode());
        RELEASE(pCloneExcept);
        ASSERT_NE(except, Exception("runtimeException"));
    }

}
