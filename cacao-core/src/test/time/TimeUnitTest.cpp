#include <gtest/gtest.h>

#include "time/TimeUnit.h"

using namespace cacao;

///// Tests unitaires :

TEST(TimeUnitTest, shouldConvert) {
    EXPECT_EQ(TimeUnit::convertIntoMillis(5, TimeUnit::SECOND), 5000);
    EXPECT_EQ(TimeUnit::convertIntoMillis(5, TimeUnit::MILLISECOND), 5);
    EXPECT_EQ(TimeUnit::convertIntoMillis(3, TimeUnit::MINUTE), 3*60*1000);
}

TEST(TimeUnitTest, shouldBeEquals) {
    TimeUnit timeUnit(1, "ms");
    EXPECT_EQ(timeUnit, TimeUnit::MILLISECOND);
    EXPECT_EQ(timeUnit.hashCode(), TimeUnit::MILLISECOND.hashCode());
    timeUnit = TimeUnit(1000, "s");
    EXPECT_EQ(timeUnit, TimeUnit::SECOND);
    EXPECT_EQ(timeUnit.hashCode(), TimeUnit::SECOND.hashCode());
}
