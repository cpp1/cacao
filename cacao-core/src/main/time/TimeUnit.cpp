#include "time/TimeUnit.h"

namespace cacao {

    const TimeUnit TimeUnit::SECOND(1000, "s");
    const TimeUnit TimeUnit::MILLISECOND(1, "ms");
    const TimeUnit TimeUnit::MINUTE((60 * 1000), "mn");

    ///// Constructeur(s) :

    TimeUnit::TimeUnit(const int convRate, const String& unit) {
        this->m_milliRate = convRate;
        this->m_unit = unit;
    }

    // Constructeur de copie
    TimeUnit::TimeUnit(const TimeUnit& timeUnit) {
        this->m_milliRate = timeUnit.m_milliRate;
    }

    ///// Méthodes de la classe Object :

    bool TimeUnit::equals(const Object& obj) const {
        if (this == &obj) {
            return true;
        }
        if (!instanceOf<TimeUnit>(&obj)) {
            return false;
        }
        const auto* pThat = dynamic_cast<const TimeUnit*>(&obj);
        bool areEqual = (this->m_milliRate == pThat -> m_milliRate);
        areEqual = areEqual && areEquals(this->m_unit, pThat->m_unit);
        return areEqual;
    }

    int TimeUnit::hashCode() const {
        int hashCode = this->m_milliRate;
        hashCode = (17 * hashCode) + hash(this->m_unit);
        return hashCode;
    }

    String TimeUnit::toString() const {
        return this->m_unit;
    }

    Object* TimeUnit::clone() const {
        return new TimeUnit(*this);
    }

    ///// Définition des opérateurs :

    TimeUnit& TimeUnit::operator=(const TimeUnit& timeUnit) {
        this->m_milliRate = timeUnit.m_milliRate;
        this->m_unit = timeUnit.m_unit;
        return *this;
    }

    ///// Méthode(s) statique(s):

    long TimeUnit::convertIntoMillis(const long value, const TimeUnit& newUnit) {
        return (value * newUnit.m_milliRate);
    }


}
