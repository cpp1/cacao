#include "boolean/Boolean.h"

namespace cacao {

    ////// Définition des champs statiques :
    const Boolean Boolean::TRUE = Boolean(true);
    const Boolean Boolean::FALSE = Boolean(false);

    ///// Définition des constructeurs :

    Boolean::Boolean(const bool boolValue) {
        this->m_value = boolValue;
    }

    Boolean::Boolean(const int intValue) {
        this->m_value = ((intValue % 2) == 1);
    }

    /// Constructeur de copie
    Boolean::Boolean(const Boolean& boolean) {
        this->m_value = boolean.m_value;
    }

    ///// Définition des opérateurs :

    Boolean& Boolean::operator=(const Boolean& boolean) noexcept {
        this->m_value = boolean.m_value;
        return *this;
    }

    Boolean& Boolean::operator=(const bool& boolValue) noexcept {
        this->m_value = boolValue;
        return *this;
    }

    Boolean& Boolean::operator=(const int& intValue) noexcept {
        this->m_value = (intValue % 2 == 1);
        return *this;
    }

    bool Boolean::operator==(const bool& boolValue) const noexcept {
        return (this->m_value == boolValue);
    }

    bool Boolean::operator==(const Boolean& boolValue) const noexcept {
        return (this->m_value == boolValue.m_value);
    }

    Boolean Boolean::operator&&(const Boolean& boolean) const noexcept {
        return {m_value && boolean.m_value};
    }

    Boolean Boolean::operator&&(const bool boolean) const noexcept {
        return {this->m_value && boolean};
    }

    Boolean Boolean::operator||(const Boolean& boolean) const noexcept {
        return {m_value || boolean.m_value};
    }

    Boolean Boolean::operator||(bool boolean) const noexcept {
        return {this->m_value || boolean};
    }

    Boolean Boolean::operator!() const noexcept {
        return {!m_value};
    }

    Boolean Boolean::operator+(const Boolean& boolean) const noexcept {
        return {this->intValue() + boolean.intValue()};
    }

    Boolean Boolean::operator*(const Boolean& boolean) const noexcept {
        return {this->intValue() * boolean.intValue()};
    }

    Boolean Boolean::operator-(const Boolean& boolean) const noexcept {
        return this->operator+(boolean);
    }

    ///// Méthode(s) de la classe Object :

    bool Boolean::equals(const Object& obj) const {
        if (this == &obj) {
            return true;
        }
        if (!instanceOf<Boolean>(&obj)) {
            return false;
        }
        const auto* pThat = dynamic_cast<const Boolean*>(&obj);
        return (this->m_value == pThat->m_value);
    }

    int Boolean::hashCode() const {
        return (this->m_value ? 1 : 0);
    }

    String Boolean::toString() const {
        return (this->m_value ? "true" : "false");
    }

    Object* Boolean::clone() const {
        return new Boolean(*this);
    }

    ///// Méthode(s) générale(s) :

    int Boolean::intValue() const noexcept {
        return (this->m_value ? 1 : 0);
    }

    bool Boolean::boolValue() const noexcept {
        return this->m_value;
    }


}