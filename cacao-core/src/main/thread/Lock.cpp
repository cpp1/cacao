#include "thread/Lock.h"

namespace cacao {

    bool SimpleLock::equals(const Object& object) const {
        if (this == &object) {
            return true;
        }
        return false;
    }

    int SimpleLock::hashCode() const {
        return 0;
    }

    String SimpleLock::toString() const {
        return {};
    }

    Object* SimpleLock::clone() const {
        return new SimpleLock();
    }

    void SimpleLock::lock() noexcept {
        this->m_mutex.lock();
    }

    void SimpleLock::unlock() noexcept {
        this->m_mutex.unlock();
    }
}