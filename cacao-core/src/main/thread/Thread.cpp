
#include "thread/Thread.h"

using namespace std;

namespace cacao {

    ///// Classe(s) statiques :

    /**
     * Méthode permettant une attente de millis millisecondes.
     * @param millis : nombre de millisecondes
     */
    void Thread::sleep(const long millis) noexcept {
        this_thread::sleep_for(chrono::milliseconds(millis));
    }


    /**
     * Méthode permettant 'attendre un temps donné
     * @param amount
     * @param timeUnit
     */
    void Thread::sleep(const long amount, const TimeUnit& timeUnit) noexcept {
        Thread::sleep(TimeUnit::convertIntoMillis(amount, timeUnit));
    }
}