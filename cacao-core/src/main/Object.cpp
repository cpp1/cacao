#include "Object.h"

using namespace std;

namespace cacao {

    bool Object::equals(const Object& obj) const {
        return (this == &obj);
    }

    int Object::hashCode() const {
        // On renvoie la valeur du pointeur :
        return static_cast<int>(reinterpret_cast<size_t>(this));
    }

    ///// Définition des opérateurs : /////

    bool Object::operator==(const Object& obj) const {
        return this->equals(obj);
    }

    bool Object::operator!=(const Object& obj) const {
        return !(this->equals(obj));
    }

    //////////////////////////
    ///// Méthodes générales :
    //////////////////////////

    bool areEquals(const Object* pObj1, const Object* pObj2) {
        if (pObj1 == nullptr && pObj2 == nullptr) {
            return true;
        } else if (pObj1 == nullptr || pObj2 == nullptr) {
            return false;
        }
        return pObj1->equals(*pObj2);
    }

    bool areEquals(const Object& obj1, const Object& obj2) {
        return areEquals(&obj1, &obj2);
    }

    int hash(const Object& obj) {
        return obj.hashCode();
    }

    int hash(const Object& obj, const Object& objects...) {
        return (17 * obj.hashCode()) + hash(objects);
    }

}