#pragma clang diagnostic push
#pragma ide diagnostic ignored "bugprone-throw-keyword-missing"

#include "exception/RuntimeException.h"

namespace cacao {

    ///// Constructeur(s) :

    RuntimeException::RuntimeException(const char* pChars) noexcept: Exception(pChars) {
        // EMPTY
    }

    RuntimeException::RuntimeException(const String& str) noexcept: Exception(str) {
        // EMPTY
    }

    RuntimeException::RuntimeException(const RuntimeException& runtimeException) : Exception(runtimeException) {
        // EMPTY
    }

    ///// Méthodes de la classe Object :

    bool RuntimeException::equals(const Object& object) const {
        if (this == &object) {
            return true;
        }
        if (!instanceOf<RuntimeException>(&object)) {
            return false;
        }
        return Exception::equals(object);
    }

    int RuntimeException::hashCode() const {
        return Exception::hashCode();
    }

    Object* RuntimeException::clone() const {
        return new RuntimeException(*this);
    }

}
#pragma clang diagnostic pop