#include "System.h"

using namespace std;

namespace cacao {

    /// Variables statiques ///
    System System::INSTANCE;

    /// Constructeur & Destructeur ///
    System::System() = default;
    System::~System() = default;

    /// Méthodes de la classe Object ///

    String System::toString() const {
        return "system";
    }

    Object* System::clone() const {
        return nullptr;
    }

    /// Méthodes statiques : ///

    System& System::getInstance() noexcept {
        return INSTANCE;
    }

}
