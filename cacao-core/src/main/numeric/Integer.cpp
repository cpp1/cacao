
#include "numeric/Integer.h"

namespace cacao {

    ///// Constructeurs :

    Integer::Integer() {
        this->m_value = 0;
    }

    Integer::Integer(const Integer& val) {
        this->m_value = val.m_value;
    }

    Integer::Integer(const int val) {
        this->m_value = val;
    }

    ///// Méthodes de la classe Object :

    bool Integer::equals(const Object& object) const {
        if (this == &object) {
            return true;
        }
        if(!instanceOf<Integer>(&object)) {
            return false;
        }
        const auto* pthat = dynamic_cast<const Integer*>(&object);
        return (this->m_value == pthat->m_value);
    }

    int Integer::hashCode() const {
        return this->m_value;
    }

    String Integer::toString() const {
        return this->m_value;
    }

    Object* Integer::clone() const {
        return new Integer(this->m_value);
    }

    ///// Définition des opérateurs :

    Integer& Integer::operator=(const Integer& newInt) {
        this->m_value = newInt.m_value;
        return *this;
    }

    Integer Integer::operator+(const Integer& intVal) const noexcept {
        return this->m_value + intVal.m_value;
    }

    Integer Integer::operator+(const int val) const noexcept {
        return this->m_value + val;
    }

    Integer& Integer::operator+=(const Integer& intVal) {
        this->m_value += intVal.m_value;
        return *this;
    }

    Integer& Integer::operator+=(const int val) {
        this->m_value += val;
        return *this;
    }

    Integer Integer::operator-(const Integer& intVal) const noexcept {
        return this->m_value - (intVal.m_value);
    }

    Integer Integer::operator-(const int val) const noexcept {
        return ((this->m_value) - val);
    }

    Integer& Integer::operator-=(const Integer& intVal) {
        this->m_value = this->m_value - (intVal.m_value);
        return *this;
    }

    Integer& Integer::operator-=(const int val) {
        this->m_value = this->m_value - val;
        return *this;
    }

    Integer Integer::operator*(const Integer& intVal) const noexcept {
        return ((this->m_value) * (intVal.m_value));
    }

    Integer Integer::operator*(const int val) const noexcept {
        return ((this->m_value) * val);
    }

    Integer Integer::toInt() const {
        return *this;
    }

    ///// Méthode(s) statique(s) :

    Integer Integer::convertFromString(const String& value) {
        return {std::stoi(value.toBaseString())};
    }


}