#ifndef CACAO_PAIR_H
#define CACAO_PAIR_H

#include "string/String.h"
#include "Object.h"

namespace cacao {

    template<typename K, typename V>
    class Pair : public virtual Object {
    private:
        K m_leftValue;
        V m_rightValue;

        ///// Constructeur(s) et destructeur
    public:

        Pair(const K leftValue, const V rightValue) {
            this->m_leftValue = leftValue;
            this->m_rightValue = rightValue;
        }

        Pair(const Pair<K, V>& pair) {
            this->m_leftValue = pair.m_leftValue;
            this->m_rightValue = pair.m_rightValue;
        }

        ~Pair() override = default;

        ///// Définition de la classe Object :

    public:

        [[nodiscard]] bool equals(const Object& obj) const override {
            if (this == &obj) {
                return true;
            }
            if (!instanceOf<Pair<K, V>>(&obj)) {
                return false;
            }
            const auto* pThat = dynamic_cast<const Pair<K, V>*>(&obj);
            bool areEquals = (this->m_leftValue == pThat -> m_leftValue);
            areEquals = areEquals && (this->m_rightValue == pThat -> m_rightValue);
            return areEquals;
        }

        [[nodiscard]] int hashCode() const override {
            int hashCode = Object::hashCode();
            return hashCode;
        }

        [[nodiscard]] String toString() const override {
            return "Pair";
        }

        [[nodiscard]] Object* clone() const override {
            return new Pair(*this);
        }

        ///// Définition des opérateurs :

    public:

        Pair<K, V>& operator=(const Pair<K, V>& pair) noexcept {
            this->m_leftValue = pair.m_leftValue;
            this->m_rightValue = pair.m_rightValue;
            return *this;
        }

    public:

        static Pair<K, V> createPair(const K leftValue, const V rightValue) {
            return Pair(leftValue, rightValue);
        }

        ///// Définition des getters :

    public:

        inline K getLeftValue() const noexcept {
            return this->m_leftValue;
        }

        inline V getRightValue() const noexcept {
            return this->m_rightValue;
        }

    };

}

#endif //CACAO_PAIR_H
