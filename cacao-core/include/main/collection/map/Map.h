#ifndef CACAO_MAP_H
#define CACAO_MAP_H

#include "Object.h"

namespace cacao {

    /**
     * Interface représentant les Map c'est à dire les
     * dictionnaires.
     */
    template<typename K, typename T>
    class Map : public virtual Object {

    public:
        virtual int size() noexcept = 0;
        virtual void put(K *key, T *val) noexcept = 0;
    };

}

#endif //CACAO_MAP_H
