#ifndef CACAO_BOOLEAN_H
#define CACAO_BOOLEAN_H

#include "Object.h"
#include "string/String.h"

namespace cacao {

    class Boolean : public virtual Object {
    public:
        static const Boolean TRUE;
        static const Boolean FALSE;

    private:
        bool m_value;

    public:
        Boolean(bool boolValue);            // NOLINT(*-explicit-constructor)
        Boolean(int intValue);              // NOLINT(*-explicit-constructor)
        Boolean(const Boolean& boolean);    // Constructeur de copie
        ~Boolean() override = default;

    public:
        Boolean& operator=(const Boolean& boolean) noexcept;
        Boolean& operator=(const bool& boolValue) noexcept;
        Boolean& operator=(const int& intValue) noexcept;
        bool operator==(const bool& boolValue) const noexcept;
        bool operator==(const Boolean& boolValue) const noexcept;
        Boolean operator&&(const Boolean& boolean) const noexcept;
        Boolean operator&&(bool boolean) const noexcept;
        Boolean operator||(const Boolean& boolean) const noexcept;
        Boolean operator||(bool boolean) const noexcept;
        Boolean operator!() const noexcept;
        Boolean operator+(const Boolean& boolean) const noexcept;
        Boolean operator*(const Boolean& boolean) const noexcept;
        Boolean operator-(const Boolean& boolean) const noexcept;

    public:
        [[nodiscard]] bool equals(const Object& obj) const override;
        [[nodiscard]] int hashCode() const override;
        [[nodiscard]] String toString() const override;
        [[nodiscard]] Object* clone() const override;

    public:
        [[nodiscard]] int intValue() const noexcept;
        [[nodiscard]] bool boolValue() const noexcept;
    };

}

#endif //CACAO_BOOLEAN_H
