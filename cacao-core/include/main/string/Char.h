#ifndef CACAO_CHAR_H
#define CACAO_CHAR_H

#include "Object.h"

namespace cacao {

    class Char : public virtual Object {
    private:
        char m_char;

    public:
        static char DIGITS[];

    public:
        Char(const char);               // NOLINT(*-avoid-const-params-in-decls, *-explicit-constructor)
        Char(const int);                // NOLINT(*-avoid-const-params-in-decls, *-explicit-constructor)
        Char(const Char&);              // Constructeur de copie
        ~Char() override = default;

    public:
        Char& operator=(const Char&);

    public:
        [[nodiscard]] bool equals(const Object&) const override;
        [[nodiscard]] int hashCode() const override;
        [[nodiscard]] String toString() const override;
        [[nodiscard]] Object* clone() const override;
    };

}

#endif //CACAO_CHAR_H
