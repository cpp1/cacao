#ifndef CACAO_STRING_H
#define CACAO_STRING_H

#include <string>

#include "Object.h"

namespace cacao {

    class String : public virtual Object {
    private:
        char* m_chars;

    public:
        String();
        String(const char*);                // NOLINT(*-explicit-constructor)
        String(char);                       // NOLINT(*-explicit-constructor)
        String(int);                        // NOLINT(*-explicit-constructor)
        String(long);                       // NOLINT(*-explicit-constructor)
        String(const std::string& str);     // NOLINT(*-explicit-constructor)
        String(const String&);              // Constructeur de copie
        ~String() noexcept override;

    public:
        String& operator=(const String&);
        String& operator=(const std::string& str);
        String& operator=(const char*);
        String& operator+(const char*);
        String& operator+(char);
        String& operator+(const String&);
        String& operator+(int);
        char operator[](int) const;

    public:
        [[nodiscard]] bool equals(const Object& object) const override;
        [[nodiscard]] int hashCode() const override;
        [[nodiscard]] Object* clone() const override;
        [[nodiscard]] String toString() const override;

    public:
        [[nodiscard]] int size() const;
        [[nodiscard]] char getCharAt(int) const;
        [[nodiscard]] bool contains(const String& str) const;
        [[nodiscard]] bool isEmpty() const;

    public:
        [[nodiscard]] char* toChars() const noexcept;
        [[nodiscard]] std::string toBaseString() const noexcept;
    };

    ///////////////////////////
    ///// Méthodes générales :
    ///////////////////////////

    int getStringSize(const char*);
}

#endif
