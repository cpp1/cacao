#ifndef CACAO_TIMEUNIT_H
#define CACAO_TIMEUNIT_H

#include "Object.h"
#include "string/String.h"

namespace cacao {

    class TimeUnit : public virtual Object {

    public:
        static const TimeUnit SECOND;
        static const TimeUnit MILLISECOND;
        static const TimeUnit MINUTE;

    private:
        int m_milliRate;
        String m_unit;

        ///// Consructeurs :

    public:
        explicit TimeUnit(int convRate, const String& unit);
        TimeUnit(const TimeUnit& timeUnit);
        ~TimeUnit() override = default;

    public:
        [[nodiscard]] bool equals(const Object& obj) const override;
        [[nodiscard]] int hashCode() const override;

    public:
        TimeUnit& operator=(const TimeUnit& timeUnit);

    public:
        [[nodiscard]] String toString() const override;
        [[nodiscard]] Object* clone() const override;

        ///// Méthode(s) statique(s) :

    public:
        static long convertIntoMillis(long value, const TimeUnit& newUnit);

    };


}

#endif //CACAO_TIMEUNIT_H
