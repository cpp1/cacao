#ifndef SYSTEM_H
#define SYSTEM_H

#include "Object.h"

#include "string/String.h"

namespace cacao {

    /**
     * Classe représentant le système.
     */
    class System : public Object {

    private:
        static System INSTANCE;

    private:
        System();
        ~System() override;

        /// Méthodes de la classe Object ///

    public:
        [[nodiscard]] Object* clone() const override;
        [[nodiscard]] String toString() const override;

    public:
        static System& getInstance() noexcept;
    };


}

#endif
