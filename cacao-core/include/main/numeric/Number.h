#ifndef CACAO_NUMBER_H
#define CACAO_NUMBER_H

#include "Object.h"
#include "numeric/Integer.h"

namespace cacao {

    class Integer;

    class Number : public virtual Object {
    public:
        virtual Integer toInt() const = 0;

    };
}

#endif //CACAO_NUMBER_H
