#ifndef CACAO_INTEGER_H
#define CACAO_INTEGER_H

#include "Object.h"
#include "string/String.h"
#include "numeric/Number.h"

namespace cacao {

    class Integer : public virtual Object, public virtual Number {
    public:
        int m_value;

    public:
        Integer();                  // NOLINT(*-explicit-constructor)
        Integer(const Integer&);    // NOLINT(*-explicit-constructor)
        Integer(int);               // NOLINT(*-explicit-constructor)

    public:
        [[nodiscard]] bool equals(const Object& object) const override;
        [[nodiscard]] int hashCode() const override;
        [[nodiscard]] String toString() const override;
        [[nodiscard]] Object* clone() const override;

    public:
        Integer& operator=(const Integer&);
        Integer operator+(const Integer&) const noexcept;
        Integer operator+(int) const noexcept;
        Integer operator-(const Integer&) const noexcept;
        Integer operator-(int) const noexcept;
        Integer operator*(const Integer&) const noexcept;
        Integer operator*(int) const noexcept;
        Integer& operator+=(const Integer&);
        Integer& operator+=(int);
        Integer& operator-=(const Integer&);
        Integer& operator-=(int);

    public:
        [[nodiscard]] Integer toInt() const override;
        [[nodiscard]] inline int toBaseInt() const {return this->m_value;};

        ///// Méthodes statiques :

    public:
        static Integer convertFromString(const String& value);

    };
}


#endif //CACAO_INTEGER_H
