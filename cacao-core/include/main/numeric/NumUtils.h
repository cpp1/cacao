#ifndef CACAO_NUMUTILS_H
#define CACAO_NUMUTILS_H

namespace cacao {

    int getNbDigits(const long);
}
#endif //CACAO_NUMUTILS_H
