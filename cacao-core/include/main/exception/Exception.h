#include <functional>

#include "Object.h"

#include "string/String.h"

using namespace std;

namespace cacao {

    class FinallyClause : public virtual Object {
    private:
        function<void()> m_clause;

    public:

        /// Constructeurs :
        explicit FinallyClause(function<void()>) noexcept;
        FinallyClause(const FinallyClause&) noexcept;
        ~FinallyClause() noexcept override;

        /// Opérateurs :
        FinallyClause& operator=(const FinallyClause&) noexcept;

        /// Méthodes de la classe Object :
        [[nodiscard]] bool equals(const Object& object) const override;
        [[nodiscard]] int hashCode() const override;
        [[nodiscard]] String toString() const override;
        [[nodiscard]] Object* clone() const override;
    };


    class Exception : public virtual Object, public std::exception {
    private:
        String m_message;

    public:
        Exception() noexcept;
        explicit Exception(const char*) noexcept;
        explicit Exception(const String&) noexcept;
        Exception(const Exception&) noexcept;
        ~Exception() noexcept override = default;

    public:
        Exception& operator=(const Exception&) noexcept;
        [[nodiscard]] bool equals(const Object& object) const override;
        [[nodiscard]] int hashCode() const override;
        [[nodiscard]] String toString() const override;
        [[nodiscard]] Object* clone() const override;

        //// Getters & Setters :
    public:
        [[nodiscard]] String getMessage() const;
        [[nodiscard]] const char* what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_NOTHROW override;
    };

}

///// Définition des macros :
#define FINALLY(fct) FinallyClause finallyClause((fct))
