#ifndef CACAO_RUNTIMEEXCEPTION_H
#define CACAO_RUNTIMEEXCEPTION_H

#include "string/String.h"
#include "Exception.h"

namespace cacao {

    class RuntimeException : public Exception {

    public:
        RuntimeException() noexcept = default;
        explicit RuntimeException(const char* pChars) noexcept;
        explicit RuntimeException(const String& str) noexcept;
        RuntimeException(const RuntimeException& runtimeException);
        ~RuntimeException() override = default;

    public:
        [[nodiscard]] bool equals(const Object& object) const override;
        [[nodiscard]] int hashCode() const override;
        [[nodiscard]] Object* clone() const override;
    };
}

#endif //CACAO_RUNTIMEEXCEPTION_H
