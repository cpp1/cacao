#ifndef CACAO_THREAD_H
#define CACAO_THREAD_H

#include <thread>

#include <Object.h>
#include "time/TimeUnit.h"

namespace cacao {

    class Thread {

    public:
        static void sleep(const long millis) noexcept;
        static void sleep(const long amount, const TimeUnit& timeUnit) noexcept;
    };

}

#endif //CACAO_THREAD_H
