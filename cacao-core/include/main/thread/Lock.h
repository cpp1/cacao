#ifndef CACAO_LOCK_H
#define CACAO_LOCK_H

#include <thread>
#include <mutex>

#include "Object.h"
#include "string/String.h"

using namespace std;

namespace cacao {

    /**
     * Interface représentant les "verrous" utilisé
     * dans la programmetion multi-threading. <br />
     */
    class Lock : public virtual Object {
    public:
        virtual void lock() noexcept = 0;
        virtual void unlock() noexcept = 0;
    };

    class SimpleLock : public virtual Lock {
    private:
        mutex m_mutex;

    public:
        [[nodiscard]] bool equals(const Object& object) const override;
        [[nodiscard]] int hashCode() const override;
        [[nodiscard]] String toString() const override;
        [[nodiscard]] Object* clone() const override;

    public:
        void lock() noexcept override;
        void unlock() noexcept override;
    };
}


#endif //CACAO_LOCK_H
