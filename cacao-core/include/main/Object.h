#ifndef CACAO_OBJECT_H
#define CACAO_OBJECT_H

#include <cstddef>
#include <typeinfo>
#include <type_traits>

#include "MainMacros.h"

using namespace std;

namespace cacao {

    class String;

    class Object {

    public:

        ///// Cosntructeur(s) & Destructeur :
        virtual ~Object() = default;

        ///// Méthodes abstraites :
        [[nodiscard]] virtual bool equals(const Object& obj) const;
        [[nodiscard]] virtual int hashCode() const;
        [[nodiscard]] virtual String toString() const = 0;
        [[nodiscard]] virtual Object* clone() const = 0;

        ///// Opérateurs :
        bool operator==(const Object& obj) const;
        bool operator!=(const Object& obj) const;
    };

    ///// Défintion des fonctions générales :

    /**
     * Méthode permettant de savoir si un objet
     *
     * @param obj
     * @return
     */
    template<typename T, typename U>
    bool instanceOf(const U* obj) {
        if (obj == nullptr) {
            return false;
        }
        return (dynamic_cast<const T*>(obj) != nullptr);
    }

    bool areEquals(const Object* pObj1, const Object* pObj2);
    bool areEquals(const Object& obj1, const Object& obj2);

    int hash(const Object& obj);
    int hash(const Object& obj, const Object& objects...);

}

#endif
